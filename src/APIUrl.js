import axios from 'axios' //MANEJO DE API REST

const mainURL = 'https://localhost:44327'; //URL API
const Login = mainURL+'/Api/login/Login';
const Dumb = mainURL+'/Api/Login/DumbResponse';
const Projects = mainURL+'/Api/projects';

export default {
  LOGIN: Login,
  DUMB: Dumb,
  PROJECTS: Projects,
  API: axios,
}