export default {
  name: 'assignments',
  components: {},
  props: {
    proyectID: String
  },
  data () {
    return {
      isCLosingInProcess: true,
      tareasProyecto: [],
      clickedNewRow: true,
      precedente:0,
      actividad:"",
      area_responsable:"",
      fecha_compromiso:"",
      fecha_real_cierre:"",
      clicked:false,
      datoCierre: ''
    }
  },
  created (){
    if(!(this.$token === '1234control')){
      this.$router.push("/login")
    }
  },
  computed: {
    isActividadCorrect(){
      if(this.actividad.trim() == "")
      {
      return false 
      }
      else{
      return true
      }
    },
    dateToday(){
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0'); // eslint-disable-line no-unused-vars
      var mm = String(today.getMonth() + 1).padStart(2, '0'); // eslint-disable-line no-unused-vars
      var yyyy = today.getFullYear(); // eslint-disable-line no-unused-vars

      return new Date().getTime();
    },
    dateWarning(){
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0'); // eslint-disable-line no-unused-vars
      var mm = String(today.getMonth() + 1).padStart(2, '0'); // eslint-disable-line no-unused-vars
      var yyyy = today.getFullYear(); // eslint-disable-line no-unused-vars

      return new Date().getTime();
    },
    isAreaCorrect(){
      if(this.area_responsable.trim() == "")
      {
      return false 
      }
      else{
      return true
      }
    },
    isRealCorrect(){
      if(this.fecha_real_cierre == "")
      {
      return false 
      }
      else{
      return true
      }
    },
    isCompromisoCorrect(){
      if(this.fecha_compromiso == "")
      {
      return false 
      }
      else{
      return true
      }
    },
    hasTasks() {
      if(this.tareasProyecto.length > 0)
      {
      return true;
      }
      else{
      return false;
      }
    },
    listPrecedentes(){
      var precedentes = [];
      this.tareasProyecto.forEach(element => {
        precedentes.push(element.tarea_by_proy_id)
      });
      return precedentes
    },
    listSelectedPrecedentes(){
      var precedentes = [];
      precedentes.push(0)
      this.tareasProyecto.forEach(element => {
        precedentes.push(parseInt(element.precedente))
      });
      return precedentes
    },
    avance(){
      if(this.tareasProyecto.length == 0){
        return 0
      }
      var completedTareas = this.tareasProyecto.filter(tarea => tarea.concluido)
      var porcentajeAvance = ((completedTareas.length * 100) / this.tareasProyecto.length).toFixed(0)
      return porcentajeAvance
    },
    maxID(){
      var precedentes = [];
      this.tareasProyecto.forEach(element => {
        precedentes.push(element.tarea_by_proy_id)
      });

      if (precedentes.length === 0) return null;
      if (precedentes.length === 1) return precedentes[0];

      this.comparer = (this.comparer || Math.max);

      var v = precedentes[0];
      for (var i = 1; i < precedentes.length; i++) {
          v = this.comparer(precedentes[i], v);    
      }
      return parseInt(v);
    }
  },
  mounted () {
    this.setdate()
    this.$store.dispatch("TAREASPROY",this.proyectID)
    .then(success => {     
      this.tareasProyecto = success
    })
    .catch(error => { 
      console.log(error)
    })
  },
  methods: {
    test(){
      alert("WORKS")
    },
    onTime(dateTask, completed, realTask){
      //Parsing the received string to date
      var fecha = new Date(parseInt(dateTask.split('-')[0]),parseInt(dateTask.split('-')[1]-1),parseInt(dateTask.split('-')[2]));
      //Parsing real closing date task
      var fechaReal = new Date(parseInt(realTask.split('-')[0]),parseInt(realTask.split('-')[1]-1),parseInt(realTask.split('-')[2]));
      //Getting today's date
      var today = new Date();
      
      if(completed === false){
        if (fecha.getTime() < today.getTime()){
          return 'color:red'
        }else{
          return 'color:rgba(0,0,0,0.5)'
        }
      }else{
        if (fechaReal.getTime() < fecha.getTime()){
          return 'color:limegreen'
        }else{
          return 'color:gold'
        }
      }
      
    },
    markAsComplete(task){
      task.concluido = true
    },
    convertDate(){
      var d = new Date('2015-03-04T00:00:00.000Z');
      var month = parseInt(d.getMonth())+1;
      console.log(d.getDay()+"/"+month+"/"+d.getFullYear());
    },
    remove(task){
      if(this.listSelectedPrecedentes.includes(task.tarea_by_proy_id))
      {
        alert("Existe una tarea relacionada con esta tarea, no puede ser eliminada")
        return 0;
      }else{

        this.$store.dispatch("DELETETAREA", task.tarea_id)
        .then(success => {  // eslint-disable-line no-unused-vars
          const index = this.tareasProyecto.indexOf(task);
          if (index > -1) {
            this.tareasProyecto.splice(index, 1);
          }
        })
      .catch(error => { 
        alert(error)
      })
    } 


      // if(this.listSelectedPrecedentes.includes(task.tarea_by_proy_id))
      // {
      //   let message = "Existe una tarea relacionada con esta tarea, no puede ser eliminada"
      //   this.$dialog.confirm(message, this.$dialogOptions)
      //           .then(function () {
                  
      //           })
      //           .catch(function () {
      //               // This will be triggered when user clicks on cancel
      //             this.$swal('Basic', 'This is Basic', 'OK');
      //           }); 
      // }else{
      //   let message = "¿Está seguro que desea eliminar la tarea con el id "+task.tarea_by_proy_id+"?";
      //   this.$dialog.confirm(message, this.$dialogOptions)
      //           .then(function () {
      //             const index = this.tareasProyecto.indexOf(task);
      //             if (index > -1) {
      //               this.tareasProyecto.splice(index, 1);
      //             }
      //           })
      //           .catch(function () {
      //               // This will be triggered when user clicks on cancel
      //               this.$swal('Basic', 'This is Basic', 'OK');
      //           }); 
      // }
    },
    setdate(){
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1;
      var yyyy = today.getFullYear();
      if(dd<10){
              dd='0'+dd
          } 
          if(mm<10){
              mm='0'+mm
          } 
      
      today = yyyy+'-'+mm+'-'+dd;
      document.getElementById("realInput").setAttribute("min", today);
      document.getElementById("compromisoInput").setAttribute("min", today);
    },
    agregarClicked(){
      this.clickedNewRow = true
    },
    addToList(){
      this.clicked = true;
      // var actividad = document.getElementById("actividadInput");
      // var area = document.getElementById("areaInput");
      // var compromiso = document.getElementById("compromisoInput");
      // var real = document.getElementById("realInput");
      // if(!this.isActividadCorrect){actividad.classList.add("empty")}else{actividad.classList.remove("empty")}
      // if(!this.isAreaCorrect){area.classList.add("empty");}else{area.classList.remove("empty")}
      // if(!this.isCompromisoCorrect){compromiso.classList.add("empty");}else{compromiso.classList.remove("empty")}
      // if(!this.isRealCorrect){real.classList.add("empty");}else{real.classList.remove("empty")}
      if(!this.isActividadCorrect || !this.isAreaCorrect || !this.isCompromisoCorrect || !this.isRealCorrect)
      {
        return 0
      }
      var tarea = {
        "tarea_id":0,
        "tarea_by_proy_id":(this.maxID)+1,
        "proyecto_id":parseInt(this.proyectID),
        "precedente":this.precedente,
        "actividad":this.actividad,
        "area_responsable":this.area_responsable,
        "concluido":false,
        "fecha_compromiso":this.fecha_compromiso,
        "fecha_real_cierre":this.fecha_real_cierre,
        "dato_cierre":" - ",
          }
      alert("created")
      this.$store.dispatch("NEWTAREA",tarea)
      .then(success => {     
        alert("WORKS") 
        this.tareasProyecto.push({
          tarea_id:0,
          tarea_by_proy_id:(this.maxID)+1,
          proyecto_id:parseInt(this.proyectID),
          precedente:this.precedente,
          actividad:this.actividad,
          area_responsable:this.area_responsable,
          concluido:false,
          fecha_compromiso:this.fecha_compromiso,
          fecha_real_cierre:this.fecha_real_cierre,
          dato_cierre:" - ",
          })
        alert("Task "+ success.tarea_by_proy_id + " added")
        this.actividad = ""
        this.area_responsable = ""
        this.fecha_compromiso = ""
        this.fecha_real_cierre = ""
        this.precedente = 0
        this.clicked = false;
      }).catch(error => { 
        console.log(error)
      })      
    }
  }
}


