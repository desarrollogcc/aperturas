import axios from 'axios'
import ScrollTop from "../ScrollToTop/index.vue"

export default{
    name: 'catalogs',
    components: { ScrollTop },
    props: {
    proyectID: String
    },
    created(){
        if(!(this.$token === '1234control')){
            this.$router.push("/login")
        }
    },
    data(){
        return{
            formActualizar: false,
            idActualizar: -1,
            descAct: '',
            clickedNewRow: false,
            searchQuery: '0',
            proyectos: [],
            editedProyecto: null,
            tiendas: [],
            temporadas: [],
            editedTemporada: null,
            formatos: [],
            editedFormato: null,
            subdirecciones: [],
            gerentes: [],
            zonas: [],
            editedZona: null,
            tiposTiendas: [],
            editedTipoTienda: null,
            tiposInmuebles: [],
            editedTipoInmueble: null,
            sortCatalogos:[
                {text: '--Seleccionar--', value: '0'}, 
                {text: 'Proyecto', value: '1'},
                {text: 'Tienda', value: '2'}, 
                {text: 'Temporada', value: '3'}, 
                {text: 'Formato tienda', value: '4'}, 
                {text: 'Subdirección', value: '5'}, 
                {text: 'Gerente de zona', value: '6'}, 
                {text: 'Zona tienda', value: '7'}, 
                {text: 'Tipo de tienda', value: '8'}, 
                {text: 'Tipo de inmueble', value: '9'}
            ]
        }
    },

    computed:{
        hasProjects() {
            if(this.proyectos.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasTiendas() {
            if(this.tiendas.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasTemporada(){
            if(this.temporadas.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasFormato(){
            if(this.formatos.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasSubdireccion(){
            if(this.subdirecciones.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasGerente(){
            if(this.gerentes.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasZona(){
            if(this.zonas.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasTipoTienda(){
            if(this.tiposTiendas.length > 0)
            {
                return true;
            }else{
                return false;
            }
        },

        hasTipoInmueble()
        {
            if(this.tiposInmuebles.length > 0)
            {
                return true;
            }else{
                return false;
            }
        }
    },

    methods:{
        getProyectos(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoProyecto")
                .then( response => {
                        this.proyectos = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e) )
        },

        getTiendas(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoTienda")
                .then( response => {
                        this.tiendas = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getTemporadas(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoTemporada")
                .then( response => {
                        this.temporadas = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getFormatos(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoFormatoTienda")
                .then( response => {
                        this.formatos = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getSubdirecciones(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoSubdireccion")
                .then( response => {
                        this.subdirecciones = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getGerentes(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoGerenteZona")
                .then( response => {
                        this.gerentes = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getZonas(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoZonaTienda")
                .then( response => {
                        this.zonas = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getTiposTiendas(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoTipoTienda")
                .then( response => {
                        this.tiposTiendas = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },

        getTiposInmueble(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/CatalogoInmueble")
                .then( response => {
                        this.tiposInmuebles = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e))
        },


        changeCatalog: function(){
            if(this.searchQuery === '1')
            {
                this.getProyectos()
            }

            if(this.searchQuery === '2')
            {
                this.getTiendas()
            }

            if(this.searchQuery === '3')
            {
                this.getTemporadas()
            }

            if(this.searchQuery === '4')
            {
                this.getFormatos()
            }

            if(this.searchQuery === '5')
            {
                this.getSubdirecciones()
            }

            if(this.searchQuery === '6')
            {
                this.getGerentes()
            }

            if(this.searchQuery === '7')
            {
                this.getZonas()
            }

            if(this.searchQuery === '8')
            {
                this.getTiposTiendas()
            }

            if(this.searchQuery === '9')
            {
                this.getTiposInmueble()
            }
        },

        agregarClicked(){
        this.clickedNewRow = !this.clickedNewRow
        },



        deleteProject(index){
            let message = "¿Está seguro que desea eliminar el proyecto "+this.proyectos[index].tipo_proyecto+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/CatalogoProyecto/"+inst.proyectos[index].proyecto_id)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===200){                           
                            inst.proyectos.splice(index, 1);
                            inst.$dialog.alert(inst.$OKDeleteMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateProject(index){
            let message = "¿Está seguro que desea modificar el proyecto "+this.proyectos[index].tipo_proyecto+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoProyecto/"+inst.proyectos[index].proyecto_id,
                    {  
                        "proyecto_id": inst.proyectos[index].proyecto_id,
                        "tipo_proyecto": inst.proyectos[index].tipo_proyecto
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveProject(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoProyecto/",
                    {  
                        "proyecto_id": 0,
                        "tipo_proyecto": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getProyectos()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        deleteTemporada(index){
            let message = "¿Está seguro que desea eliminar la temporada "+this.temporadas[index].nombre_temporada+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/CatalogoTemporada/"+inst.temporadas[index].temporada_id)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===200){                          
                            inst.temporadas.splice(index, 1);
                            inst.$dialog.alert(inst.$OKDeleteMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateTemporada(index){
            let message = "¿Está seguro que desea modificar la temporada "+this.temporadas[index].nombre_temporada+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoTemporada/"+inst.temporadas[index].temporada_id,
                    {  
                        "temporada_id": inst.temporadas[index].temporada_id,
                        "nombre_temporada": inst.temporadas[index].nombre_temporada
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveTemporada(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoTemporada/",
                    {  
                        "temporada_id": 0,
                        "nombre_temporada": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getTemporadas()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        deleteFormato(index){
            let message = "¿Está seguro que desea eliminar el formato tienda "+this.formatos[index].nombre_formato_tienda+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/CatalogoFormatoTienda/"+inst.formatos[index].formato_tienda_id)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===200){                          
                            inst.formatos.splice(index, 1);
                            inst.$dialog.alert(inst.$OKDeleteMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateFormato(index){
            let message = "¿Está seguro que desea modificar el formato tienda "+this.formatos[index].nombre_formato_tienda+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoFormatoTienda/"+inst.formatos[index].formato_tienda_id,
                    {  
                        "formato_tienda_id": inst.formatos[index].formato_tienda_id,
                        "nombre_formato_tienda": inst.formatos[index].nombre_formato_tienda
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveFormato(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoFormatoTienda/",
                    {  
                        "formato_tienda_id": "0",
                        "nombre_formato_tienda": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getFormatos()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        deleteZona(index){
            let message = "¿Está seguro que desea eliminar la zona tienda "+this.zonas[index].nombre_zona_tienda+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/CatalogoZonaTienda/"+inst.zonas[index].zona_tienda_id)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        inst.zonas.splice(index, 1);
                        inst.$dialog.alert(inst.$OKDeleteMessage)   
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateZona(index){
            let message = "¿Está seguro que desea modificar la zona tienda "+this.zonas[index].nombre_zona_tienda+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoZonaTienda/"+inst.zonas[index].zona_tienda_id,
                    {  
                        "zona_tienda_id": inst.zonas[index].zona_tienda_id,
                        "nombre_zona_tienda": inst.zonas[index].nombre_zona_tienda
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveZona(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoZonaTienda/",
                    {  
                        "zona_tienda_id": "0",
                        "nombre_zona_tienda": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getZonas()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        deleteTipo(index){
            let message = "¿Está seguro que desea eliminar el tipo de tienda "+this.tiposTiendas[index].nombre_tipo_tienda+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/CatalogoTipoTienda/"+inst.tiposTiendas[index].tipo_tienda_id)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===200){
                            inst.tiposTiendas.splice(index, 1);
                            inst.$dialog.alert(inst.$OKDeleteMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateTipo(index){
            let message = "¿Está seguro que desea modificar el tipo de tienda "+this.tiposTiendas[index].nombre_tipo_tienda+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoTipoTienda/"+inst.tiposTiendas[index].tipo_tienda_id,
                    {  
                        "tipo_tienda_id": inst.tiposTiendas[index].tipo_tienda_id,
                        "nombre_tipo_tienda": inst.tiposTiendas[index].nombre_tipo_tienda
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveTipo(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoTipoTienda/",
                    {  
                        "tipo_tienda_id": "0",
                        "nombre_tipo_tienda": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getTiposTiendas()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        deleteTipoInmueble(index){            
                let message = "¿Está seguro que desea eliminar el tipo de inmueble "+this.tiposInmuebles[index].nombre_tipo_inmueble+"?";
                let inst = this;
                
                this.$dialog.confirm(message, this.$dialogDeleteOptions)
                    .then(function () {
                        axios
                        .delete("https://localhost:44327/api/CatalogoInmueble/"+inst.tiposInmuebles[index].tipo_inmueble_id)
                        .then(({status, response}) => { // eslint-disable-line no-unused-vars
                            if(status===200){
                                inst.tiposInmuebles.splice(index, 1);
                                inst.$dialog.alert(inst.$OKDeleteMessage)
                            }    
                        })
                        .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                        })
                    .catch(function () {
    
                    });
        },

        updateTipoInmueble(index){
            let message = "¿Está seguro que desea modificar el tipo de inmueble "+this.tiposInmuebles[index].nombre_tipo_inmueble+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/CatalogoInmueble/"+inst.tiposInmuebles[index].tipo_inmueble_id,
                    {  
                        "tipo_inmueble_id": inst.tiposInmuebles[index].tipo_inmueble_id,
                        "nombre_tipo_inmueble": inst.tiposInmuebles[index].nombre_tipo_inmueble
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveTipoInmueble(descripcion){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/CatalogoInmueble/",
                    {  
                        "tipo_inmueble_id": "0",
                        "nombre_tipo_inmueble": descripcion
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getTiposInmueble()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },



        editProyecto(index){
            this.idActualizar = index
            this.descAct = this.proyectos[index].tipo_proyecto
            this.formActualizar = true
        },

        editTemporada(index){
            this.idActualizar = index
            this.descAct = this.temporadas[index].nombre_temporada
            this.formActualizar = true
        },

        editFormato(index){
            this.idActualizar = index
            this.descAct = this.formatos[index].nombre_formato_tienda
            this.formActualizar = true
        },

        editZona(index){
            this.idActualizar = index
            this.descAct = this.zonas[index].nombre_zona_tienda
            this.formActualizar = true
        },

        editTipoTienda(index){
            this.idActualizar = index
            this.descAct = this.tiposTiendas[index].nombre_tipo_tienda
            this.formActualizar = true
        },

        editTipoInmueble(index){
            this.idActualizar = index
            this.descAct = this.tiposInmuebles[index].nombre_tipo_inmueble
            this.formActualizar = true
        }
    }
}