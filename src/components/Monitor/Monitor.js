import Tasks from "../Tasks/index.vue"
import Assignments from "../Assignments/index.vue"
import LoadingWheel from "../LoadingWheel/index.vue"
import ScrollTop from "../ScrollToTop/index.vue"
import axios from 'axios'
export default {
  name: 'monitor',
  props: [],
  components: {
    Tasks,
    Assignments,
    LoadingWheel,
    ScrollTop
  },
  data () {
    return {
      // DATA FROM THE PROJECT
      dataProyecto:{
        proyecto: "",      
        fechaApertura: "",
        fechaInicio: "",
        tienda: "",
        nombreTienda: "",
        formato: "",
        subdireccion: "",
        gerente: "",
        espejo: "",
        tipoTienda: "",
        inmueble: "",
        zonaTienda: "",
        temporada: "",
        direccionTienda: "",
        observacionesEspejo: "",
        observacionesGral: "",
      },
      //TASKS 
      tasksColumns: ["#","Precedente","Actividad","Área Responsable","Concluido","Fecha Compromiso","Fecha de Cierre","Evidencia","Dato de Cierre"],
      existingData: false,
      //CATALOGOS
      CAT_Proyecto: [],
      CAT_FormatoTienda: [],
      CAT_Gerente: [],
      CAT_Subdireccion: [],
      CAT_Temporada: [],
      CAT_Tienda: [],
      CAT_Inmueble: [],
      CAT_Zona: [],
      CAT_TipoTienda: [],
      hasLoaded: false,
      
    }
  },
  computed: {
    isApertura: function(){
      if(this.dataProyecto.proyecto == "2"){
        return false;
      }else{
        return true;
      }
    }
  },
  created() {
    if(!(this.$token === '1234control')){
      this.$router.push("/login")
    }else{
      this.getProyectos();
      this.getFormato();
      this.getSubdireccion();
      this.getGerente();
      this.getInmueble();
      this.getTipoTienda();
      this.getTienda();
      this.getTemporada();
      this.getZona();
      this.hasLoaded = true;
    }
  },
  mounted () {
    

        this.setdate();
        if(!(this.$route.params.ID == undefined))
        {
          axios
          .get("https://localhost:44327/api/Proyectos/"+this.$route.params.ID)
          .then( response => {
            if(!Object.keys(response.data).length){
              alert("Empty Data Set")
              this.existingData = false
            }else{
              this.existingData = true
              this.fillFieldsWithData(response.data)
            }
          })
          .catch( e => alert("Proyecto no existente... "+e) )
        }
    
  },
  methods: {
    setdate(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
        
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("inputFechaApertura").setAttribute("min", today);
        document.getElementById("inputFechaInicioObra").setAttribute("min", today);
      },
    send(){
        alert("OK")
    },
    fechaFooter(){   
      var tomorrow = new Date();
      tomorrow.setTime(tomorrow.getTime() + (1000*3600*24));       
      document.getElementById("spanDate").innerHTML = tomorrow.getFullYear();
    },
    regresarMenuPrincipal(){
      this.$router.push("/").then(() => this.$scrollToTop());      
    },
    sendComunicado(){
      alert("This button works")
    }, 
    fillFieldsWithData(ProyectoData){
      // console.log(ProyectoData)
      this.dataProyecto.proyecto = ProyectoData.TipoProyecto  
      this.dataProyecto.fechaApertura = ProyectoData.FechaApertura.substring(0,10)
      this.dataProyecto.fechaInicio = ProyectoData.FechaInicioObra.substring(0,10)
      this.dataProyecto.tienda = ProyectoData.Tienda
      this.dataProyecto.nombreTienda = ProyectoData.NombreTienda
      this.dataProyecto.formato = ProyectoData.Formato
      this.dataProyecto.subdireccion = ProyectoData.Subdireccion
      this.dataProyecto.gerente = ProyectoData.GerenteZona
      this.dataProyecto.espejo = ProyectoData.TiendaEspejo
      this.dataProyecto.tipoTienda = ProyectoData.TipoTienda
      this.dataProyecto.inmueble = ProyectoData.TipoInmueble
      this.dataProyecto.zonaTienda = ProyectoData.ZonaTienda
      this.dataProyecto.temporada = ProyectoData.Temporada
      this.dataProyecto.direccionTienda = ProyectoData.Direccion
      this.dataProyecto.observacionesEspejo = ProyectoData.ObservacionesEspejo
      this.dataProyecto.observacionesGral = ProyectoData.ObservacionesGral
    },
    postProyecto(){
      var datos = {
        "proyecto_id": 0,
        "tipo_proyecto": parseInt(this.dataProyecto.proyecto),
        "fecha_apertura": this.dataProyecto.fechaApertura,
        "fecha_inicio_obra": this.dataProyecto.fechaInicio,
        "tienda": parseInt(this.dataProyecto.tienda),
        "nombre_tienda": this.dataProyecto.nombreTienda,
        "formato_tienda": parseInt(this.dataProyecto.formato),
        "subdireccion": parseInt(this.dataProyecto.subdireccion),
        "gerente_zona": parseInt(this.dataProyecto.gerente),
        "tienda_espejo": parseInt(this.dataProyecto.espejo),
        "tipo_tienda": parseInt(this.dataProyecto.tipoTienda),
        "tipo_inmueble": parseInt(this.dataProyecto.inmueble),
        "zona_tienda": parseInt(this.dataProyecto.zonaTienda),
        "temporada": parseInt(this.dataProyecto.temporada),
        "direccion_tienda": this.dataProyecto.direccionTienda,
        "observaciones_espejo": this.dataProyecto.observacionesEspejo,
        "observaciones_generales": this.dataProyecto.observacionesGral,
        "avance": 0,
        "estado": 1,
        "en_comunicado_gral": false,
      }
      console.log(datos)
      this.$store.dispatch("POSTPROYECTO", datos )
      .then(success => { 
        // this.$route.pushRoute("/tareas/proyecto="+success.proyecto_id)
        console.log(success)
        this.$router.push("/").then(this.$router.push("/tareas/proyecto="+success.proyecto_id));  //CHECAR EL REDIRECCIONAMIENTO
        })
      .catch(error => { 
        alert(error)
      })
    },
    getProyectos(){
      this.$store.dispatch("PROYECTOS")
      .then(success => {         
        this.CAT_Proyecto = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getFormato(){
      this.$store.dispatch("FORMATOS")
      .then(success => { 
        this.CAT_FormatoTienda = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getGerente(){
      this.$store.dispatch("GERENTE")
      .then(success => {       
        this.CAT_Gerente = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getSubdireccion(){
      this.$store.dispatch("SUBDIRECCION")
      .then(success => {  
        this.CAT_Subdireccion = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getTemporada(){
      this.$store.dispatch("TEMPORADA")
      .then(success => {          
        this.CAT_Temporada = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getTienda(){
      this.$store.dispatch("TIENDA")
      .then(success => {         
        this.CAT_Tienda = success
      })
      .catch(error => {
        alert(error)
      })
    },
    getInmueble(){
      this.$store.dispatch("INMUEBLE")
      .then(success => {  
        this.CAT_Inmueble = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getZona(){
      this.$store.dispatch("ZONA")
      .then(success => {   
        this.CAT_Zona = success
      })
      .catch(error => { 
        alert(error)
      })
    },
    getTipoTienda(){
      this.$store.dispatch("TIPOTIENDA")
      .then(success => {    
        this.CAT_TipoTienda = success
      })
      .catch(error => { 
        alert(error)
      })
    }
  }
}
