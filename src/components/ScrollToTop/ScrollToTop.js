import $ from 'jquery'
export default {
  name: 'scrolltotop',
  components: {},
  props: [],
  data () {
    return {

    }
  },
  computed: {

  },
  mounted () {
    const btnScrollToTop = document.querySelector("#btnScrollToTop");

    btnScrollToTop.addEventListener("click", function() {
        // window.scrollTo(0,0);
        // window.scrollTo({
        //     top: 0,
        //     left: 0,
        //     behavior: "smooth"
        // });
        $("html, body").animate({scrollTop: 0}, "slow");
    });
  },
  methods: {

  }
}


