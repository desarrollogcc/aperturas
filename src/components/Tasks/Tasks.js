import TaskItem from '../task-item.vue'

export default {
  name: 'tasks',
  components: {
    TaskItem,
  },
  props: {
    columns: Array,
  },
  data () {
    return {
      newTodo: '',
      idForTodo: 4,
      beforeEditCache: '',
      filter: "all",
      todos: [
        {
          'id': 1,
          'title': 'Finish Vue Screencast',
          'completed': false,
          'editing': false,
        },
        {
          'id': 2,
          'title': 'To take over the world',
          'completed': false,
          'editing': false,
        },
        {
          'id': 3,
          'title': 'Finish this todo list',
          'completed': false,
          'editing': false,
        }
      ]
    }
  },
  // directives: {
  //   focus: {
  //
  //     inserted: function (el) {
  //       el.focus()
  //     }
  //   }
  // },
  computed: {
    remaining(){
      return this.todos.filter(todo => !todo.completed).length
    },
    anyRemaining(){
      return this.remaining != 0
    },
    todosFiltered(){
      if(this.filter == 'all'){
        return this.todos
      } else if(this.filter == 'active'){
        return this.todos.filter(todo => !todo.completed)
      } else if(this.filter == 'completed'){
        return this.todos.filter(todo => todo.completed)
      }
      
      return this.todos
    },
    showClearCompletedButton(){
      return this.todos.filter(todo => todo.completed).length > 0
    }
  },
  mounted () {

  },
  methods: {
    addTodo(){
      if(this.newTodo.trim().length == 0){
        return
      }
      this.todos.push({
        id: this.idForTodo,
        title: this.newTodo,
        completed: false,
        editing:false,
      })
      this.newTodo = ''
      this.idForTodo++
    },
    removeTodo(index)
    {
      this.todos.splice(index,1)
    },
                // editTodo(todo){
                //   if(todo.completed == true)
                //   {
                //     return
                //   }
                //   this.beforeEditCache = todo.title
                //   todo.editing = true;
                // },
                // doneEdit(todo){
                //   if(todo.title.trim().length == 0){
                //     todo.title = this.beforeEditCache
                //   }
                //   todo.editing = false
                // },
                // cancelEdit(todo){
                //   todo.title = this.beforeEditCache
                //   todo.editing = false
                // },
    checkAllTodos(){
      this.todos.forEach((todo) => todo.completed = event.target.checked);
    },
    clearCompleted(){
      this.filter = 'all'
      this.todos = this.todos.filter(todo => !todo.completed)
    },
    finishedEdit(data){
      this.todos.splice(data.index, 1, data.todo)
    }
  }
}


