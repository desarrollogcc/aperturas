import axios from 'axios'
import ScrollTop from "../ScrollToTop/index.vue"

export default{
    name: 'users',
    components: { ScrollTop },
    
    created(){
        if(!(this.$token === '1234control')){
            this.$router.push("/login")
        }
    },

    data(){
        return{
            clickedNewRow: false,
            usuarios: [],
            formActualizar: false,
            idActualizar: -1,
            nombreActualizar: '',
            searchPerfil: 'A',
            searchCadena: 'A',
            searchArea: 'A',
            sortPerfil:[
                {text: 'Administrador', value: 'A'},
                {text: 'Funcional', value: 'F'}
            ]
        }
    },

    mounted(){
        this.getUsers()
    },

    computed:{
        hasUsers(){
            if(this.usuarios.length > 0){
                return true
            }
            else{
                return false
            }
        }
    },

    methods: {
        agregarClicked(){
            this.clickedNewRow = !this.clickedNewRow
        },

        getUsers(){
            let inst = this;
            axios
                .get("https://localhost:44327/api/Usuarios")
                .then( response => {
                        this.usuarios = response.data
                    })
                .catch( e => inst.$dialog.alert(inst.$NotRows+e) )
        },

        deleteUser(index){
            let message = "¿Está seguro que desea eliminar el empleado "+this.usuarios[index].no_empleado+"?";
            let inst = this;
            
            this.$dialog.confirm(message, this.$dialogDeleteOptions)
                .then(function () {
                    axios
                    .delete("https://localhost:44327/api/Usuarios/"+inst.usuarios[index].no_empleado)
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===200){                           
                            inst.usuarios.splice(index, 1);
                            inst.$dialog.alert(inst.$OKDeleteMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotDeleteMessage+e) )
                    })
                .catch(function () {

                });
        },

        updateUser(index){
            let message = "¿Está seguro que desea modificar el registro del empleado "+this.usuarios[index].no_empleado+"?";
            let inst = this;

            this.$dialog.confirm(message, this.$dialogModifyOptions)
                .then(function () {
                    axios
                    .put("https://localhost:44327/api/Usuarios/"+inst.usuarios[index].no_empleado,
                    {  
                        "no_empleado": inst.usuarios[index].no_empleado,
                        "nombre": inst.usuarios[index].nombre,
                        "email": inst.usuarios[index].email,
                        "perfil": inst.usuarios[index].perfil,
                        "cadena": inst.usuarios[index].cadena,
                        "area_asignada": inst.usuarios[index].area_asignada,
                        "activo": inst.usuarios[index].activo,
                        "reportes": inst.usuarios[index].reportes,
                        "empleado_usuario": inst.usuarios[index].empleado_usuario,
                        "password": inst.usuarios[index].password
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===204){
                            inst.formActualizar = false
                            inst.$dialog.alert(inst.$OKModifyMessage)
                        }    
                    })
                    .catch( e => inst.$dialog.alert(inst.$NotModifyMessage+e) )
                    })
                .catch(function () {

                });
        },

        saveUser(no_empleado,nombre,email, searchPerfil,searchCadena,searchArea,activo,reportes,empleado_usuario,password){
            let inst = this;
            this.$dialog.confirm(this.$AddMessage, this.$dialogAddOptions)
                .then(function () {
                    axios
                    .post("https://localhost:44327/api/Usuarios/",
                    {  
                        "no_empleado": no_empleado,
                        "nombre": nombre,
                        "email": email,
                        "perfil": searchPerfil,
                        "cadena": searchCadena,
                        "area_asignada": searchArea,
                        "activo": activo,
                        "reportes": reportes,
                        "empleado_usuario": empleado_usuario,
                        "password": password
                    })
                    .then(({status, response}) => { // eslint-disable-line no-unused-vars
                        if(status===201){
                            inst.$dialog.alert(inst.$OKAddMessage)
                            inst.getUsers()
                            inst.clickedNewRow = false
                        }    
                    })
                    .catch( e =>inst.$dialog.alert(inst.$NotAddMessage+e) )
                    })
                .catch(function () {

                });
        },

        editUser(index){
            // Antes de mostrar el formulario de actualizar, rellenamos sus campos
            this.idActualizar = index;
            this.nombreActualizar = this.usuarios[index].nombre;
            this.formActualizar = true;
        }
    }
}