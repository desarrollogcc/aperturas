//BOOTSTRAP
import 'bootstrap/dist/css/bootstrap.css'

//VUEX
import store from './store'

import Vue from 'vue'

//LAYOUTS
import Master from './components/layouts/MasterPage.vue'
import LoginEmpty from './components/layouts/LoginLayout.vue'
import Dashboard from './components/layouts/DashboardLayout.vue'
import NotFound from './components/layouts/NotFoundLayout.vue'

//CONSTANTES

//AXIOS
import axios from 'axios' //MANEJO DE API REST
Vue.prototype.$http = axios
Vue.prototype.$scrollToTop = () => window.scrollTo(0, 0)

Vue.prototype.$token = "1234control"
// Vue.prototype.$token = "1234control"
// Vue.prototype.$token = "wrongtoken"

Vue.prototype.$OKDeleteMessage = "Se eliminó el registro correctamente."
Vue.prototype.$NotDeleteMessage = "No se pudo eliminar el registro: "
Vue.prototype.$OKModifyMessage = "Se actualizó el registro correctamente."
Vue.prototype.$NotModifyMessage = "No se pudo actualizar el registro: "
Vue.prototype.$OKAddMessage = "Se agregó el registro correctamente."
Vue.prototype.$NotAddMessage = "No se pudo agregar el registro: "
Vue.prototype.$NotRows = "No se pueden cargar los registros: "
Vue.prototype.$AddMessage = "¿Está seguro que desea agregar el registro?"
Vue.prototype.$dialogDeleteOptions = { html: false, // set to true if your message contains HTML tags. eg: "Delete <b>Foo</b> ?"
                                loader: false, // set to true if you want the dailog to show a loader after click on "proceed"
                                reverse: true, // switch the button positions (left to right, and vise versa)
                                okText: 'Eliminar',
                                cancelText: 'Cancelar',
                                animation: 'bounce', // Available: "zoom", "bounce", "fade"
                                type: 'basic', // coming soon: 'soft', 'hard'
                                backdropClose: false, // set to true to close the dialog when clicking outside of the dialog window, i.e. click landing on the mask
                                customClass: '' // Custom class to be injected into the parent node for the current dialog instance
}
Vue.prototype.$dialogModifyOptions = { html: false, // set to true if your message contains HTML tags. eg: "Delete <b>Foo</b> ?"
                                loader: false, // set to true if you want the dailog to show a loader after click on "proceed"
                                reverse: true, // switch the button positions (left to right, and vise versa)
                                okText: 'Modificar',
                                cancelText: 'Cancelar',
                                animation: 'bounce', // Available: "zoom", "bounce", "fade"
                                type: 'basic', // coming soon: 'soft', 'hard'
                                backdropClose: false, // set to true to close the dialog when clicking outside of the dialog window, i.e. click landing on the mask
                                customClass: '' // Custom class to be injected into the parent node for the current dialog instance
}
Vue.prototype.$dialogAddOptions = { html: false, // set to true if your message contains HTML tags. eg: "Delete <b>Foo</b> ?"
                                loader: false, // set to true if you want the dailog to show a loader after click on "proceed"
                                reverse: true, // switch the button positions (left to right, and vise versa)
                                okText: 'Agregar',
                                cancelText: 'Cancelar',
                                animation: 'bounce', // Available: "zoom", "bounce", "fade"
                                type: 'basic', // coming soon: 'soft', 'hard'
                                backdropClose: false, // set to true to close the dialog when clicking outside of the dialog window, i.e. click landing on the mask
                                customClass: '' // Custom class to be injected into the parent node for the current dialog instance
}
                    
axios.defaults.baseURL = 'https://localhost:44327/Api/'
//VUETIFY
import Vuetify from "vuetify"

//CONFIGURACION ROUTER
import VueRouter from 'vue-router'
import routes from './routes/routes.js'

//CONFIRM DIALOG
import VuejsDialog from 'vuejs-dialog';
//import VuejsDialogMixin from 'vuejs-dialog/dist/vuejs-dialog-mixin.min.js'; // only needed in custom components
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
// Tell Vue to install the plugin.
Vue.use(VuejsDialog);


//SWEET ALERT
//import swal from 'vue-sweetalert2';
//import 'sweetalert2/dist/sweetalert2.min.css';
//Vue.use(swal);
//Vue.prototype.$swal = swal;
import VueSwal from 'vue-swal'
 Vue.use(VueSwal)


Vue.component('dashboard-layout', Dashboard);
Vue.component('login-empty-layout',LoginEmpty);
Vue.component('not-found-layout', NotFound);

Vue.use(VueRouter)
Vue.use(Vuetify)

const router = new VueRouter({
  routes,
  mode:'history'
})

Vue.config.productionTip = false

new Vue({
  render: h => h(Master),
  router: router,
  store,
}).$mount('#app')
