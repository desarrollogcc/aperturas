const routes = [
    {  
        //ROUTE TO LOGIN
        path: '/login',
        name: 'login',
        meta: { layout: "login-empty-layout"},
        component: () => import('../components/Login/index.vue')
    },
    { 
        //ROUTE TO THE DEFAULT PAGE
        path: '/',
        name: 'home',
        meta: { layout: "dashboard-layout" },
        component: () => import('../App.vue')
    },
    { 
        //ROUTE TO THE PROJECT WITH THE TASKS
        path: '/tareas/proyecto=:ID',
        meta: { layout: "dashboard-layout" },
        name: 'tareas',
        component: () => import("../components/Monitor/index.vue")
    },
    { 
        //ROUTE TO THE PROJECT WITH THE TASKS
        path: '/tareas/',
        meta: { layout: "dashboard-layout" },
        name: 'tasksNew',
        component: () => import("../components/Monitor/index.vue")
    },
    {   
        //ROUTE TO THE PROJECT WITH THE TASKS
        path: '/test/',
        meta: { layout: "dashboard-layout" },
        name: 'testing',
        component: () => import("../components/TestingComponent/index.vue")
    },
    {   //ROUTE TO THE CATALOG PAGE
        path: '/catalogos/',
        meta: { layout: "dashboard-layout" },
        name: 'catalog',
        component: () => import('../components/Catalog/index.vue')
    },
    {   
        //NOT FOUND ROUTE
        path: '*',
        meta: { layout: "not-found-layout" },
        name: 'NotFound',
        component: () => import("../components/NotFound/index.vue")
    },
    {   //ROUTE TO THE USERS PAGE
        path: '/usuarios/',
        meta: { layout: "dashboard-layout" },
        name: 'users',
        component: () => import('../components/Users/index.vue')
    }
]
export default routes