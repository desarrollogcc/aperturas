import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'

import User from './stores/modules/user.js'
import Proyects from './stores/modules/proyects.js'
import Assignments from './stores/modules/assignments.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user: User,
    proyects: Proyects,
    assignments: Assignments
  }
})