import axios from 'axios'

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        TAREASPROY: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('TareaProyectos/'+payload)
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        NEWTAREA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.post('TareaProyectos/',payload)
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        console.log("WORKD")
                        resolve(data)
                        
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        DELETETAREA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.delete('TareaProyectos/'+payload)
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        console.log("WORKD")
                        resolve(data)
                        
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
    }
}
