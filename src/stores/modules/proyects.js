import axios from 'axios'

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        FORMATOS: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoFormatoTienda')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        PROYECTOS: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoProyecto')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    console.log(error)
                    reject(error);
                })
            });
        },
        GERENTE: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoGerenteZona')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        SUBDIRECCION: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoSubdireccion')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        TEMPORADA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoTemporada')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        TIENDA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoTienda')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        INMUEBLE: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoInmueble')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        ZONA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoZonaTienda')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        TIPOTIENDA: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.get('CatalogoTipoTienda')
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(data)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        POSTPROYECTO: ({commit}, payload) => {  // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.post('Proyectos',payload)
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 201){
                        resolve(data)
                    }
                })
                .catch(error => {
                    alert("ENTERED IN ERROR CODE")
                    reject(error);
                })
            });
        },
    }
}