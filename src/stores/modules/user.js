import axios from 'axios'

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        LOGIN: ({commit}, payload) => { // eslint-disable-line no-unused-vars
            return new Promise((resolve, reject) => {
                axios.post('Login', payload)
                .then(({data,status}) => { // eslint-disable-line no-unused-vars
                    if(status === 200){
                        resolve(true)
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        }
    }
}